//
//  TaskDisplayViewController.m
//  TaskHistory
//
//  Created by 原嘉章 on 2016/09/05.
//  Copyright © 2016年 原嘉章. All rights reserved.
//

#import "TaskDisplayViewController.h"
#import "UIView+Toast.h"

@interface TaskDisplayViewController ()
@property (weak, nonatomic) IBOutlet UIPickerView *taskPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *taskPickerHeight;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet UITextField *necessaryTime;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *taskName;
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;
@end

@implementation TaskDisplayViewController
{
    NSArray* categoryArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    NSLog(@"cview2.bounds: %@", NSStringFromCGRect(self.view.bounds));
    NSLog(@"cview2.frame : %@", NSStringFromCGRect(self.view.frame));
    // textfieldの設定
    self.timeTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.timeTextField.delegate = self;
    self.timeTextField.returnKeyType = UIReturnKeyDone;

    
    // pickerdelegateの設定
    self.taskPicker.delegate = self;

    // pickerを隠す
    self.taskPickerHeight.constant = 0;

    //カテゴリを配列に
    categoryArray = [NSArray arrayWithObjects:
                     @"eng.1",
                     @"eng.2",
                     @"eng.3",
                     @"eng.4",
                     @"eng.5",
                     @"eng.6", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchSubmitButton:(id)sender {
    if(![self.taskName.text isEqualToString:NSLocalizedString(@"default_task_name", nil)]){
        if([self checkInputText:self.timeTextField.text]){
            NSLog(@"Success");
            // 送信処理
            
        }else{
            NSLog(@"Failure");
            
            // トースト表示
            NSString *message = NSLocalizedString(@"toast_error_number", nil);
            [self.view makeToast:message duration:2.0f position:@"center"];
        }
    }else{
        NSString *message = NSLocalizedString(@"toast_error_task", nil);
        [self.view makeToast:message duration:2.0f position:@"center"];
    }
}

- (IBAction)touchChooseButton:(id)sender {
    if( self.taskPickerHeight.constant == 0 ){
        self.taskPickerHeight.constant = 216.f;
        [self.chooseButton setTitle:@"決定" forState:UIControlStateNormal];

        // タスクがデフォルト値だったら、Arrayのデフォルト値をラベル表示
        if([self.taskName.text isEqualToString:NSLocalizedString(@"default_task_name", nil)]){
            [self.taskName setText:categoryArray[0]];
        }
    }else{
        self.taskPickerHeight.constant = 0;
        [self.chooseButton setTitle:@"選択" forState:UIControlStateNormal];
    }

}


// ピッカービューのコンポーネント（行）の数を返す *必須
- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

// 行数を返す例　*必須
- (NSInteger) pickerView: (UIPickerView*)pView numberOfRowsInComponent:(NSInteger) component {
    NSUInteger cnt = [categoryArray count];
    return cnt;
}

// ピッカービューの行のタイトルを返す
- (NSString*)pickerView: (UIPickerView*) pView titleForRow:(NSInteger) row forComponent:(NSInteger)componet
{
    //n行目に配列のn番目の要素を設定
    return [categoryArray objectAtIndex:row];
}

//選択されたピッカービューを取得
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //0列目の選択している行番号を取得
    NSInteger selectedRow = [pickerView selectedRowInComponent:0];
    [self.taskName setText:categoryArray[selectedRow]];
     NSLog(@"%ld", (long)selectedRow);
}

//Returnボタンがタップされた時に呼び出される
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.timeTextField resignFirstResponder];
    return YES;
}

// UITextField で入力を受け付けるかどうかを判断する UITextFieldDelegate メソッド
- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string
{
    BOOL result;
    
    // 文字数を 3 文字に制限
    NSUInteger maxLength = 3;
    
    // Return キーが押された場合
    if ([string compare:@"\n"] == 0)
    {
        // 文字数に限らず、それを受け入れる
        result = YES;
    }
    else
    {
        // Return キーではない場合には、最大文字数を超えないときだけ、受け入れる
        NSUInteger textLength = textField.text.length;
        NSUInteger rangeLength = range.length;
        NSUInteger stringLength = string.length;
        
        NSUInteger length = textLength - rangeLength + stringLength;
        
        result = (length <= maxLength);
    }
    
    return result;
}

/*
 * 入力値が数字かどうか判定する
 * string:チェックする文字列
 */
- (BOOL)checkInputText:(NSString *)string
{
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:string];
    return [[NSCharacterSet decimalDigitCharacterSet] isSupersetOfSet:characterSet];
}


@end