//
//  AppDelegate.h
//  TaskHistory
//
//  Created by 原嘉章 on 2016/09/05.
//  Copyright © 2016年 原嘉章. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

