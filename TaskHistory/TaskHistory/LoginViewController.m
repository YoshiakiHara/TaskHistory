//
//  LoginViewController.m
//  TaskHistory
//
//  Created by 原嘉章 on 2016/09/05.
//  Copyright © 2016年 原嘉章. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchLoginButton:(id)sender {
    // IDを指定してSegueを呼び出します。
    // IDを条件分岐することによって2つ目のViewと入れ替える事ができます。
    [self performSegueWithIdentifier:@"taskDisplayView" sender:self];
}

@end
