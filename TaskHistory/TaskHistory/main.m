//
//  main.m
//  TaskHistory
//
//  Created by 原嘉章 on 2016/09/05.
//  Copyright © 2016年 原嘉章. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
